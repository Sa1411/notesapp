package com.example.todoapp.Database

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import com.example.todoapp.Models.Note


@Dao
interface NoteDao {
    //jsbajxbajshgkaj
    @Insert
    suspend fun insert(note: Note)

    @Delete
    suspend fun delete(note:Note)

    @Query("SELECT * From notes_table order by id ASC")
    fun getAllNotes():LiveData<List<Note>>

    @Query("UPDATE notes_table Set title= :title, note= :note where id= :id")
    suspend fun update(id:Int?,title:String?,note:String?)
}