package com.example.todoapp
import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.ContactsContract.CommonDataKinds.Note
import android.widget.Toast
import com.example.todoapp.databinding.ActivityCreateNoteBinding
import java.text.SimpleDateFormat
import java.util.*

class CreateNote : AppCompatActivity() {
    private lateinit var binding:ActivityCreateNoteBinding
    private lateinit var note: Note
    private lateinit var old_note:Note
    var isUpdate = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        try {
            old_note = intent.getSerializableExtra("currentNote")as Note
            binding.etTitle.setText(old_note.toString())
            binding.etNote.setText(old_note.toString())
            isUpdate = true
        }catch (e:Exception){
            e.printStackTrace()
        }
        binding.imgCheck.setOnClickListener{
            val title=binding.etTitle.text.toString()
            val note=binding.etNote.text.toString()

            if(title.isNotEmpty() || note.isNotEmpty()){
                val formatter=SimpleDateFormat("EEE,d MMM YYY HH:mm a")

                if (isUpdate){
                    note = Note(
                        
                    )

                }

                val intent = Intent()
                intent.putExtra("note",note)
                setResult(Activity.RESULT_OK,intent)
                finish()
            }
            else{
                Toast.makeText(this, "please fill Data ", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
        }
        binding.backArrow.setOnClickListener{
            onBackPressed()
        }
    }
}


