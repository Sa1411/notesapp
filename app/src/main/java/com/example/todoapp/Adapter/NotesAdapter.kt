package com.example.todoapp.Adapter

import android.content.Context
import android.os.Parcel
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.example.todoapp.Models.Note
import com.example.todoapp.R
import kotlin.random.Random

class NotesAdapter(private val context:Context,val listener:NoteItemClickListener): RecyclerView.Adapter<NotesAdapter.NoteViewHolder>(){

    private val NoteList=ArrayList<Note>()
    private val Fulllist=ArrayList<Note>()




    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NotesAdapter.NoteViewHolder {
        return NoteViewHolder(
            LayoutInflater.from(context).inflate(R.layout.list_item,parent,false)
        )
    }

    override fun onBindViewHolder(holder: NotesAdapter.NoteViewHolder, position: Int) {
       val currentNote=NoteList[position]
        holder.title.text=currentNote.title
        holder.note_tv.text=currentNote.note
        holder.date.text= currentNote.date
        holder.title.isSelected= true
        holder.date.isSelected= true


        holder.notes_layout.setCardBackgroundColor(holder.itemView.resources.getColor(randomColour(),null))
        holder.notes_layout.setOnClickListener{
            listener.onItemClicked(NoteList[holder.adapterPosition])
        }
        holder.notes_layout.setOnLongClickListener{
            listener.onLongItemClicked(NoteList[holder.adapterPosition],holder.notes_layout)
            true
        }
    }

    override fun getItemCount(): Int {
        return NoteList.size
    }
    fun updateList(newList: List<Note>){
        Fulllist.clear()
        Fulllist.addAll(newList)

        NoteList.clear()
        NoteList.addAll(Fulllist)
        notifyDataSetChanged()
    }

    fun filterList(search:String){
        NoteList.clear()
        for (item in Fulllist){
            if(item.title?.lowercase()?.contains(search.lowercase())==true ||
                        item.note?.lowercase()?.contains(search.lowercase())==true){
                NoteList.add(item)
            }
        }
        notifyDataSetChanged()
    }

    fun randomColour():Int{
        val list=ArrayList<Int>()
        list.add(R.color.NoteColour1)
        list.add(R.color.NoteColour2)
        list.add(R.color.NoteColour6)
        list.add(R.color.NoteColour5)
        list.add(R.color.NoteColour4)
        list.add(R.color.NoteColour3)

        val seed = System.currentTimeMillis().toInt()
        val randomIndex= Random(seed).nextInt(list.size)
        return list[randomIndex]
    }


inner class NoteViewHolder(itemView: View):RecyclerView.ViewHolder(itemView) {
    val notes_layout = itemView.findViewById<CardView>(R.id.card_item)
    val title = itemView.findViewById<TextView>(R.id.tv_title)
    val note_tv = itemView.findViewById<TextView>(R.id.tv_note)
    val date = itemView.findViewById<TextView>(R.id.tv_date)
}
    interface NoteItemClickListener{
        fun onItemClicked(note: Note)
        fun onLongItemClicked(note: Note,cardView: CardView)
    }
}






